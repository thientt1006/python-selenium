import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import pandas as pd

browser = webdriver.Chrome(executable_path="./chromedriver.exe")
browser.maximize_window()

def find_website_by_keywords(URL, keywords):
    browser.get(URL)

    search = browser.find_element_by_xpath(
        "//input[@title='Tìm kiếm']")
    search.send_keys(keywords)
    search.send_keys(Keys.ENTER)
    time.sleep(3)

def get_list_titles():
    list_titles = []
    titles = browser.find_elements_by_xpath(
        "//div[@class='yuRUbf']//h3[@class='LC20lb DKV0Md']")
    time.sleep(1)

    for i in range(0, int(len(titles))):
        list_titles.append(titles[i].text)
    return list_titles

def get_list_links():
    list_links = []
    links = browser.find_elements_by_xpath(
        "//div[@class='yuRUbf']/a")
    time.sleep(1)

    for i in range(0, int(len(links))):
        list_links.append(links[i].get_attribute("href"))
    return list_links


def get_top_result(stop):
    list1 = []
    list2 = []
    
    list_titles = get_list_titles()
    list_links = get_list_links()

    start = 0
    for i in range(0, len(list_titles)):
            if (start != stop):
                if (list_titles[i] != ""):
                    list1.append(list_titles[i])
                    list2.append(list_links[i])
                    start = start + 1
            else:
                break
    return list1, list2

def main():
    URL = "https://www.google.com/"
    keywords = "python selenium tutorial"
    # stop = input("Enter number of title you want to get: ")
    stop = 6

    find_website_by_keywords(URL, keywords)

    a, b = get_top_result(stop)

    m = pd.DataFrame(a, columns=['Title'])
    n = pd.DataFrame(b, columns=['Link'])

    df = pd.concat([m, n], axis=1)
    print(df)
    # df.to_csv("Top_Search.csv", index=False)
    browser.close()

if __name__ == "__main__":
    main()
