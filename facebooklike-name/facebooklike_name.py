import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common import keys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


def warm_up_chrome():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")

    browser = webdriver.Chrome(chrome_options=chrome_options)

    browser.maximize_window()
    return browser


def login_facebook(browser):
    browser.get("https://vi-vn.facebook.com")

    email = browser.find_element_by_id('email')
    email.send_keys("user@gmail.com")
    time.sleep(1)

    password = browser.find_element_by_id('pass')
    password.send_keys("pass")
    time.sleep(1)

    btnLogin = browser.find_element_by_name('login')
    btnLogin.send_keys(Keys.ENTER)
    time.sleep(5)


def find_friend_id(browser):
    findfriend = browser.find_element_by_xpath(
        "//input[@aria-label='Tìm kiếm trên Facebook']")
    findfriend.click()
    findfriend.send_keys('thanh', Keys.SPACE,'lucario')
    findfriend.send_keys(Keys.ENTER)
    time.sleep(3)

    findfriend1 = browser.find_element_by_xpath(
        "//a[@aria-label='Thanh Lucario']")
    findfriend1.click()
    time.sleep(2)


def like_posts(browser):
    browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(3)
    
    likes = browser.find_elements_by_xpath(
        "//div[@class='tvfksri0 ozuftl9m jmbispl3 olo4ujb6']//div[@aria-label='Thích']")
    time.sleep(3)

    for i in range(0, 6):
        upgrade_actions = ActionChains(browser)
        upgrade_actions.move_to_element(likes[i])
        upgrade_actions.click()
        upgrade_actions.perform()
        time.sleep(1)


def go_to_friend_page(browser):
    browser.get("https://www.facebook.com/profile.php?id=100010526381189")
    time.sleep(2)

    email = browser.find_element_by_xpath(
        "//input[@aria-label='Email hoặc điện thoại']")
    email.send_keys("user@gmail.com")
    time.sleep(1)

    password = browser.find_element_by_xpath("//input[@aria-label='Mật khẩu']")
    password.send_keys("pass")
    time.sleep(1)

    btnLogin = browser.find_element_by_xpath(
        "//div[@aria-label='Accessible login button']")
    btnLogin.send_keys(Keys.ENTER)
    time.sleep(3)

    browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(3)

    likes = browser.find_elements_by_xpath(
        "//div[@class='tvfksri0 ozuftl9m jmbispl3 olo4ujb6']//div[@aria-label='Viết bình luận']")
    time.sleep(3)

    for i in range(0, 6):
        upgrade_actions = ActionChains(browser)
        upgrade_actions.move_to_element(likes[i])
        upgrade_actions.perform()
        time.sleep(1)
    time.sleep(5)


def main():
    a = input("Enter the number: \n0: Login FB, find your friend id and like first 6 posts. \n1: Find your friend id \n")

    while (a != str(0)) & (a != str(1)):
        a = input("Error: \nEnter the number again: ")

    browser = warm_up_chrome()

    if int(a) == 0:
        login_facebook(browser)
        find_friend_id(browser)
        like_posts(browser)
    else:
        go_to_friend_page(browser)
    
    browser.close()


if __name__ == "__main__":
    main()
